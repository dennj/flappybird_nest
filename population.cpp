#include "population.h"

#include <cassert>
#include <execution>
#include <iostream>
#include <random>
#include <ranges>
#include <vector>

Population::Population(std::size_t size) {
  if (!penguin_texture.loadFromFile("penguin.png")) {
    assert(true);
  }

  for (std::size_t i = 0; i < size; ++i) {
    penguins.emplace_back(penguin_texture);
  }
}

void Population::Show(sf::RenderWindow& render_window) {
  for (auto& penguin : penguins) {
    penguin.Show(render_window);
  }
}

void Population::Update(int height, sf::FloatRect obst_up,
                        sf::FloatRect obs_down) {
  std::for_each(std::execution::par, penguins.begin(), penguins.end(),
                [&height, &obst_up, &obs_down](auto& penguin) {
                  penguin.Update(height, obst_up, obs_down);
                });
}

// returns whether all the penguins are dead
bool Population::get_all_dead() const {
  return std::all_of(penguins.begin(), penguins.end(),
                     [](const auto& penguin) { return penguin.get_is_dead(); });
}

// gets the next generation of penguins
void Population::NaturalSelection() {
  std::vector<Penguin> new_penguins;

  for (std::size_t i = 0; i < penguins.size(); ++i) {
    new_penguins.emplace_back(penguin_texture);
  }

  SetBestPenguin();
  CalculateFitnessSum();

  // the champion lives on always
  new_penguins.front() = penguins.at(best_penguin).GenerateChild();
  new_penguins.front().set_is_best();

  for (auto& new_penguin : new_penguins) {
    // select parent based on fitness
    const Penguin parent = SelectParent();
    // get baby from them
    new_penguin = parent.GenerateChild();
  }

  penguins = new_penguins;
  ++generation_counter;
}

void Population::CalculateFitnessSum() {
  fitness_sum = 0;

  for (const Penguin& penguin : penguins) {
    fitness_sum += penguin.get_score();
  }
}

// Randomly chose parent
// Higher is the score and higher is the chance of being selected
Penguin Population::SelectParent() {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis(0.0F, fitness_sum);
  const float rand = dis(gen);

  float running_sum = 0;
  for (auto& penguin : penguins) {
    running_sum += penguin.get_score();
    if (running_sum > rand) {
      return penguin;
    }
  }

  assert(true);
  // Ensures(false);  // should never get to this point
}

// mutates all the brains of the babies
void Population::MutateBabies() {
  std::for_each(std::execution::par, penguins.begin(), penguins.end(),
                [](auto& penguin) { penguin.Mutate(); });
}

// Finds the penguin with the highest fitness and sets it as the best penguin
void Population::SetBestPenguin() {
  float max = 0;
  std::size_t maxIndex = 0;
  for (std::size_t i = 0; i < penguins.size(); i++) {
    if (penguins.at(i).get_score() > max) {
      max = penguins.at(i).get_score();
      maxIndex = i;
    }
  }

  best_penguin = maxIndex;
}
