#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

class Obstacle {
 public:
  Obstacle(float max_height, float width);

  void Show(sf::RenderWindow &render);
  void Update();

  sf::FloatRect get_up_bounds() const { return obstacle_up.getGlobalBounds(); }
  sf::FloatRect get_down_bounds() const {
    return obstacle_down.getGlobalBounds();
  }

 private:
  float width = 2000;
  float pos_y = 2000;

  sf::RectangleShape obstacle_up;
  sf::RectangleShape obstacle_down;

  sf::Color dark_green{0, 100, 0};
};
