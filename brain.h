#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <array>
#include <complex>
#include <vector>

class Brain {
 public:
  Brain(std::size_t number_neurons);
  void Randomize();

  // returns a perfect copy of this brain object
  Brain Clone() const;

  // mutates the brain by setting some random values
  void Mutate();

  void UpdateSkinColor();  // Generate RGB color from synapse values

  sf::Color get_color() const { return skin_color; }
  bool get_accelleration(std::vector<float> input) const;

  std::vector<float> MatrixVectorMultiplication(std::vector<float> M,
                                                std::vector<float> v) const;

 private:
  std::vector<float> NN;
  sf::Color skin_color{255, 255, 255};
};
