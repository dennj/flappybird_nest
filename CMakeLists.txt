cmake_minimum_required(VERSION 3.16)

project(FlappyPenguin CXX)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_EXTENSIONS OFF ) # gnu extensions off
set(CMAKE_CXX_STANDARD_REQUIRED ON) # c++20 standard is not an optional

# Define sources and executable
set(EXECUTABLE_NAME "FlappyPenguin")
add_executable(${EXECUTABLE_NAME} main.cpp penguin.cpp brain.cpp population.cpp obstacle.cpp)

# Detect and add SFML
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})

find_package(SFML 2 REQUIRED network audio graphics window system)
if(SFML_FOUND)
  include_directories(${SFML_INCLUDE_DIR})
  target_link_libraries(${EXECUTABLE_NAME} PUBLIC sfml-graphics sfml-audio sfml-window sfml-system ${SFML_DEPENDENCIES})
endif()

# Copy texture
configure_file(${CMAKE_SOURCE_DIR}/penguin.png ${CMAKE_CURRENT_BINARY_DIR}/penguin.png COPYONLY)
