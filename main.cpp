#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <iostream>

#include "obstacle.h"
#include "population.h"

struct Score {
  int score;
  sf::Color color;
};

int main() {
  Population pop{100};
  constexpr int width = 2000;
  constexpr int height = 2000;

  sf::RenderWindow window(sf::VideoMode(width, height), "Flappy Penguin");
  window.setFramerateLimit(25);  // Slow

  Obstacle obs(height, width);
  int score_counter = 0;
  int last_score = 0;
  int best_score = 1;

  std::vector<Score> score_log{{0, sf::Color::White}};

  bool show_scene = true;

  while (window.isOpen()) {
    window.clear(sf::Color::Cyan);

    obs.Update();
    obs.Show(window);

    if (pop.get_all_dead()) {  // genetic algorithm
      obs = Obstacle(height, width);
      pop.NaturalSelection();
      pop.MutateBabies();
      last_score = score_counter;

      if (score_counter > best_score) best_score = score_counter;
      score_log.emplace_back(score_counter, pop.get_skin_color_best());
      score_counter = 0;

    } else {
      pop.Update(height, obs.get_up_bounds(), obs.get_down_bounds());

      pop.Show(window);
      ++score_counter;
    }

    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) window.close();

      if (event.type == sf::Event::KeyPressed) {
        if (event.key.code == sf::Keyboard::R) {
          // Reset
          obs = Obstacle(height, width);
          pop.NaturalSelection();
          pop.MutateBabies();
          last_score = score_counter;

          if (score_counter > best_score) best_score = score_counter;
          score_log.emplace_back(score_counter, pop.get_skin_color_best());
          score_counter = 0;
        }

        if (event.key.code == sf::Keyboard::S) {
          window.setFramerateLimit(25);  // Slow
        }

        if (event.key.code == sf::Keyboard::F) {
          window.setFramerateLimit(0);  // Fast
        }

        if (event.key.code == sf::Keyboard::Space) {
          show_scene = !show_scene;  // Plot graph
          window.setFramerateLimit(0);
        }
      }
    }

    if (show_scene) {
      window.display();
    } else {
      window.clear();
      for (std::size_t i = 0; i < score_log.size(); ++i) {
        sf::CircleShape pixel(10);
        pixel.setFillColor(score_log.at(i).color);
        pixel.setPosition(
            {static_cast<float>(i * 30),
             (1 - static_cast<float>(score_log.at(i).score) / best_score) *
                 height});
        window.draw(pixel);
      }
      if (!show_scene) {
        window.display();
      }
    }

    const std::string title =
        "Flappy Penguin!   Score: " + std::to_string(score_counter) +
        " last: " + std::to_string(last_score) +
        " best: " + std::to_string(best_score) +
        "   [Generation: " + std::to_string(pop.get_generation_counter()) + "]";
    window.setTitle(title);
  }

  return 0;
}
