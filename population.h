#pragma once

#include "penguin.h"

class Population {
 public:
  Population(std::size_t size);
  void Show(sf::RenderWindow &render_window);
  void Update(int height, sf::FloatRect obst_up, sf::FloatRect obs_down);
  bool get_all_dead() const;  // returns whether all the penguins are dead
  void NaturalSelection();    // gets the next generation of penguins
  void CalculateFitnessSum();
  Penguin SelectParent();
  void MutateBabies();  // mutates all the brains of the babies
  void SetBestPenguin();
  int get_generation_counter() const { return generation_counter; }
  sf::Color get_skin_color_best() const {
    return penguins.at(best_penguin).get_skin_color();
  }

 private:
  std::vector<Penguin> penguins;
  float fitness_sum{0};
  int generation_counter{1};
  std::size_t best_penguin{0};  // index of the best penguin of the population
  sf::Texture penguin_texture;
};
