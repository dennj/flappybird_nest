#include "obstacle.h"

#include <random>

Obstacle::Obstacle(float max_height, float width) {
  std::random_device rd;
  std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()

  float hole_size = 600;
  std::uniform_real_distribution<float> dis(500.0F,
                                            max_height - 500 - hole_size);

  pos_y = dis(gen);

  float obstacle_length = 100;
  obstacle_up.setSize({obstacle_length, pos_y});

  obstacle_up.setFillColor(dark_green);

  obstacle_down.setSize({obstacle_length, max_height - pos_y});
  obstacle_down.setFillColor(dark_green);

  obstacle_up.setPosition(width, 0);
  obstacle_down.setPosition(width, pos_y + hole_size);
}

void Obstacle::Show(sf::RenderWindow &render) {
  render.draw(obstacle_up);
  render.draw(obstacle_down);
}

void Obstacle::Update() {
  obstacle_up.move(-10, 0);
  obstacle_down.move(-10, 0);

  if (obstacle_up.getPosition().x < 50) {
    std::random_device rd;
    std::mt19937 gen(
        rd());  // Standard mersenne_twister_engine seeded with rd()

    float hole_size = 600;
    std::uniform_real_distribution<float> dis(100.0F, 2000 - 100 - hole_size);

    pos_y = dis(gen);

    float obstacle_length = 200;
    obstacle_up.setSize({obstacle_length, pos_y});
    sf::Color DarkGreen(0, 100, 0);
    obstacle_up.setFillColor(DarkGreen);

    obstacle_down.setSize({obstacle_length, 2000 - pos_y});
    obstacle_down.setFillColor(DarkGreen);

    obstacle_up.setPosition(width, 0);
    obstacle_down.setPosition(width, pos_y + hole_size);
  }
}
