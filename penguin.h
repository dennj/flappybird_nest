#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <cmath>

#include "brain.h"

class Penguin {
 public:
  Penguin(const sf::Texture &txt);
  Penguin(Brain b, const sf::Texture &txt);
  void Show(sf::RenderWindow &render);
  void Jump();
  void Update(float height, sf::FloatRect obst_up, sf::FloatRect obst_down);
  void CalculateFitness(sf::Vector2f goal);
  Penguin GenerateChild() const;
  void Mutate();

  void set_dead(bool d);
  bool get_is_dead() const { return dead; }
  void set_is_best();
  float get_score() const { return score; }

  sf::Color get_skin_color() const { return brain.get_color(); }

 private:
  sf::Vector2f starting_pos{500.0F, 1000.0F};
  float vel{0.0F};

  Brain brain;

  bool dead = false;
  bool reached_goal = false;
  bool is_best = false;  // is the best penguin from the previous generation?
  float score = 0;       // Time alive

  sf::Texture texture;
  sf::Sprite sprite_penguin;

  float Norm(sf::Vector2f vec) const;
};
