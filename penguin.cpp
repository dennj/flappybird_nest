#include "penguin.h"

#include <cassert>
#include <cmath>
#include <iostream>

Penguin::Penguin(const sf::Texture& txt) : Penguin(Brain(10), txt) {}

Penguin::Penguin(Brain b, const sf::Texture& txt)
    : brain(std::move(b)), texture(txt) {
  sprite_penguin.setPosition(starting_pos);
  sprite_penguin.setRotation(30);
}

// draws the dot on the screen
void Penguin::Show(sf::RenderWindow& render) {
  sprite_penguin.setTexture(texture);
  sprite_penguin.setColor(brain.get_color());

  if (!dead) render.draw(sprite_penguin);
}

void Penguin::Jump() { vel = -30; }

// Elaborate the inputs and check for collisions
void Penguin::Update(float height, sf::FloatRect obst_up,
                     sf::FloatRect obst_down) {
  if (!dead) {
    const float normalized_y = sprite_penguin.getGlobalBounds().top / height;
    const float normalized_vel = vel / 1000;
    const float normalized_dis_obstacle_x =
        (obst_down.left - sprite_penguin.getGlobalBounds().left) / height;
    const float normalized_dis_obstacle_y =
        (obst_down.top - sprite_penguin.getGlobalBounds().top) / height;

    const std::vector<float> input{1, normalized_y, normalized_vel,
                                   normalized_dis_obstacle_x,
                                   normalized_dis_obstacle_y};

    if (brain.get_accelleration(input)) {
      Jump();
    }
    vel += 5;  // gravity

    sprite_penguin.move({0, vel});

    // if near the edges of the window then kill it
    if (sprite_penguin.getPosition().y < 20 ||
        sprite_penguin.getPosition().y > height - 20) {
      dead = true;
    } else if (obst_up.intersects(sprite_penguin.getGlobalBounds())) {
      dead = true;  // Collision with obstacle
    } else if (obst_down.intersects(sprite_penguin.getGlobalBounds())) {
      dead = true;
    } else {
      ++score;
    }
  }
}

// clone it
Penguin Penguin::GenerateChild() const {
  // babies have the same brain as their parents
  return Penguin(brain.Clone(), texture);
}

void Penguin::Mutate() { brain.Mutate(); }

void Penguin::set_dead(bool d) { dead = d; }

void Penguin::set_is_best() { is_best = true; }

float Penguin::Norm(sf::Vector2f vec) const {
  return std::sqrt(std::pow(vec.x, 2) + std::pow(vec.y, 2));
}
