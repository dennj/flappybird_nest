#include "brain.h"

#include <algorithm>
#include <cassert>
#include <random>
#include <ranges>

Brain::Brain(std::size_t number_neurons) {
  NN.resize(number_neurons * number_neurons);
  Randomize();
}

// sets all the vectors in directions to a random vector with length 1
void Brain::Randomize() {
  std::random_device rd;
  std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()

  std::uniform_real_distribution<float> dis(-1.0F, 1.0F);

  std::ranges::for_each(NN,
                        [&dis, &gen](auto& synapse) { synapse = dis(gen); });

  UpdateSkinColor();
}

// returns a perfect copy of this brain object
Brain Brain::Clone() const {
  assert(NN.size() > 6);
  assert(NN.size() < 1000);

  Brain clone{NN.size()};
  clone.NN = NN;
  return clone;
}

// mutates the brain by setting some of the directions to random vectors
void Brain::Mutate() {
  std::random_device rd;
  std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<float> dis(0.0F, 1.0F);
  std::uniform_real_distribution<float> circle_dis(0.0F, 3.14F * 2.0F);

  // chance that any vector in directions gets changed
  constexpr float mutationRate = 0.01F;

  for (auto& synapse : NN) {
    const float rand = dis(gen);
    if (rand < mutationRate) {
      synapse = circle_dis(gen);
    }
  }

  UpdateSkinColor();
}

void Brain::UpdateSkinColor() {
  // Find biggerst element for normalize data
  const float n1 = NN.at(NN.size() - 1);
  const float n2 = NN.at(NN.size() - 2);
  const float n3 = NN.at(NN.size() - 3);
  const float biggerst = std::max({std::abs(n1), std::abs(n2), std::abs(n3)});

  // translate synapses weight in a value between 0 and 255
  skin_color.r = static_cast<uint8_t>((n1 / biggerst + 1) / 2 * 255);
  skin_color.g = static_cast<uint8_t>((n2 / biggerst + 1) / 2 * 255);
  skin_color.b = static_cast<uint8_t>((n3 / biggerst + 1) / 2 * 255);
}

bool Brain::get_accelleration(std::vector<float> input) const {
  assert(input.size() * input.size() <= NN.size());

  if (input.size() * input.size() < NN.size())
    input.resize(std::sqrt(NN.size()));

  std::vector<float> res = MatrixVectorMultiplication(NN, input);

  // Treshold function ReLu
  std::for_each(res.begin(), res.end(), [](auto& v) { v = v > 0 ? v : 0; });

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis(-0.1F, 0.1F);
  std::for_each(res.begin(), res.end(),
                [&dis, &gen](auto& v) { v += dis(gen); });

  res = MatrixVectorMultiplication(NN, res);

  // Treshold function ReLu
  // std::for_each(res.begin(), res.end(), [](auto& v) { v = v > 0 ? v : 0; });

  return res.at(0) > 0;
}

std::vector<float> Brain::MatrixVectorMultiplication(
    std::vector<float> M, std::vector<float> v) const {
  const std::size_t size = v.size();

  assert(size * size == M.size());

  std::vector<float> result;
  result.resize(size);

  for (std::size_t col = 0; col < size; ++col) {
    for (std::size_t row = 0; row < size; ++row) {
      result.at(col) += v.at(row) * M.at(size * col + row);
    }
  }

  assert(size == result.size());
  return result;
}
